namespace :grab do
  task :results => :environment do
    ResultsFixtures::ResultsFixtures.new.perform
  end
end