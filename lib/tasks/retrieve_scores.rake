namespace :grab do
 task :scores => :environment do
  Prediction.all.each do |prediction|
    score = points_total prediction, prediction.result
    allocate_points prediction, score
  end
 end
end

def points_total(prediction, result)
 wrong_predictions = [prediction.home_score - result.home_score, prediction.away_score - result.away_score]
 wrong_predictions = wrong_predictions.reject { |i| i == 0 }.size # returns 0, 1 or 2
  case wrong_predictions
   when 0 then 3
   when 1 then 1
   else 0
  end
end

 def allocate_points(prediction, score)
  prediction.update_attributes!(score: score)
 end