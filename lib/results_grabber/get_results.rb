require 'open-uri'
require 'nokogiri'

module ResultsGrabber::GetResults

RESULTS_URL = "http://www.bbc.co.uk/sport/football/results/partial/competition-118996114?structureid=5"

def get_results # Get me all results
 doc = Nokogiri::HTML(open(RESULTS_URL))
 days = doc.css('.table-header').each do |h2_tag|
 date = Date.parse(h2_tag.text.strip).to_date
  matches = h2_tag.xpath('following-sibling::*[1]').css('tr.report')
  matches.each do |match|
    home_team = match.css('.team-home').text.strip
    away_team = match.css('.team-away').text.strip
    score = match.css('.score').text.strip
    home_score, away_score = score.split("-").map(&:to_i)
    fixture = Fixture.where(fixture_date: date, home_team: home_team, away_team: away_team).first
    prediction_array = Prediction.where(fixture_id: fixture)

    pred = prediction_array.map {|p| p.id}
    pred.each do |p|
      pred_id = p
    Result.where(home_team: home_team, away_team: away_team, score: score, fixture_date: date, home_score: home_score, away_score: away_score, prediction_id: pred_id).first_or_create!
    end
    end
  end
end


end