require 'open-uri'
require 'nokogiri'

module LeagueGrabber::GetTable

def get_league_table # Get me Premier League Table
  url = 'http://www.bbc.co.uk/sport/football/tables'
  doc = Nokogiri::HTML.parse(open url)
  teams = doc.search('tbody tr.team')


  keys = teams.first.search('td').map do |k|
    k['class'].gsub('-', '_').to_sym
  end

  hsh = teams.flat_map do |team|
    Hash[keys.zip(team.search('td').map(&:text))]
  end

  hsh.map  do |k|
    position = k[:position].gsub(/[Nomovement ]/, "")
    team_name = k[:team_name]
    points = k[:points]
    goal_difference = k[:goal_difference]
    played = k[:played]

    League.create!(position: position, team_name: team_name, points: points, goal_difference: goal_difference, played: played)
  end


  
end
end

