module ResultsFixtures
  class ResultsFixtures
    include ResultsGrabber::GetResults

    def perform #rake task method
      get_results
    end
   end
  end
