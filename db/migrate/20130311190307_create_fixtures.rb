class CreateFixtures < ActiveRecord::Migration
  def change
    create_table :fixtures do |t|
    	t.string :home_team
    	t.string :away_team
    	t.text :fixture_date
    	t.text :kickoff_time
      t.integer :prediction_id

      t.timestamps
    end
  end
end
