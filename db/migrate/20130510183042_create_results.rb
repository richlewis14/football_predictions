class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.string :home_team
      t.string :away_team
      t.string :score
      t.integer :home_score
      t.integer :away_score
      t.date :fixture_date
      t.integer :prediction_id

      t.timestamps
    end
  end
end
