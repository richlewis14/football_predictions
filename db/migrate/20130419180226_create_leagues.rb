class CreateLeagues < ActiveRecord::Migration
  def change
    create_table :leagues do |t|
      t.integer :position
      t.string :team_name
      t.integer :played
      t.integer :goal_difference
      t.integer :points

      t.timestamps
    end
  end
end
