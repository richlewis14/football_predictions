# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:

 set :environment, "development"
 set :output, "/home/richard/Rails/football_predictions/log/cron_log.log"

#Retrieve League Table
 every :day, :at => '12:20am' do
   rake "grab:league"
 end

 #Retrieve League fixtures
 every :day, :at => '12:20am' do
   rake "grab:fixtures"
 end

