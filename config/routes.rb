FootballPredictions::Application.routes.draw do

  
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :predictions, :only => [:index, :new, :create]
  resources :fixtures, :only => [:index, :new, :create]
  resources :leagues, :only => [:new, :create]
  resources :results, :only => [:new, :create]
  resources :mini_leagues, :only => [:index, :new, :create]
  resources :teams, :only => [:index, :new, :create]
  resources :enrollments, :only => [:index, :new, :create]


  scope :controller => :predictions do
    get 'index'

  end
 

  scope :controller => :main do
    get 'index'
    get 'fixturelist'
    get 'leaguetable'
  end

  scope :controller => :members do
    get 'index'
  end

  # Directing the user after login
  authenticated :user do
    root :to => 'members#index'
  end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'main#index'


  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
