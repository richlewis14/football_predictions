class MainController < ApplicationController
  include LeagueGrabber::GetTable

	def index
    
	end

	def fixturelist
	  @fixtures = Fixture.all
	  @fixture_date = @fixtures.group_by {|fd| fd.fixture_date }

	end

  def leaguetable
    @league = League.all
  end
  
end
