class MembersController < ApplicationController
  before_filter :authenticate_user!
  def index

    #Fixtures for the next 3 days
    three_days_from_today = Date.today + 3
    @fixtures = Fixture.where("fixture_date <= ?", three_days_from_today)
    @fixture_date = @fixtures.group_by {|fd| fd.fixture_date}

    #Result from the last 3 days
    previous_three_days = Date.today - 3
    @results = Result.where("fixture_date >= ?", previous_three_days).group("home_team, away_team, fixture_date")
    @result_date = @results.group_by {|r| r.fixture_date}

    #Members Points
    @weekly_points = current_user.predictions.where("fixture_date >= ?", Date.today - 3).sum(:score)


    #Members Total Points
    @total_points = current_user.predictions.sum(:score)

    #High Scores
    @user = User.all.sort{|a, b| b.sum_score <=> a.sum_score}

    
  end

end
