class PredictionsController < ApplicationController
  before_filter :authenticate_user!

  def index
    previous_three_days = Date.today - 3
    @predictions = current_user.predictions.where("fixture_date >= ?", previous_three_days)
    @user_predictions = @predictions.group_by {|p| p.fixture_date}
  end

	def new
     @prediction = Prediction.new
     
	end

	def create
    begin
      params[:predictions].each do |prediction|
        prediction.merge!({:user_id => current_user.id})
        Prediction.new(prediction).save!
      end
      redirect_to root_path, :notice => 'Predictions Submitted Successfully'
    rescue
      render 'new'
    end
  end


end
