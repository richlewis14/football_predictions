class ResultsController < ApplicationController
  def new
    @result = Result.new
  end
 
  def create
   @result = Result.new(params[:result])
    if @result.save
      redirect_to root_path
    else
      render :new
    end
  end
end
