class Prediction < ActiveRecord::Base
   attr_accessible :home_team, :away_team, :home_score, :away_score, :fixture_date, :fixture_id, :user_id, :score

   belongs_to :user
   has_one :fixture
   has_one :result
end
