class Result < ActiveRecord::Base
   attr_accessible :home_team, :away_team, :fixture_date, :home_score, :away_score, :prediction_id, :score
end
