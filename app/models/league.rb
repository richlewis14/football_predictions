class League < ActiveRecord::Base
  attr_accessible :position, :team_name, :played, :goal_difference, :points
end
