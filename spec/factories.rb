FactoryGirl.define do
  factory :user do
    email "joebloggs@gmail.com"
    password  "testpassword"
    password_confirmation "testpassword"
  end
end