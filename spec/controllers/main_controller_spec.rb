require 'spec_helper'

describe MainController do

  describe "visit Home Page" do
    
    it "should display Home page" do
      get :index
      response.should be_success
      response.should render_template(:index)
    end
  end

  describe "visit Fixturelist Page" do
    
    it "should display Fixturelist page" do
      get :fixturelist
      response.should be_success
      response.should render_template(:fixturelist)
    end
  end

end